package com.akademiakodu.preferences;

import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.first_name)
    protected EditText mName;
    @BindView(R.id.last_name)
    protected EditText mLastName;
    @BindView(R.id.sex)
    protected RadioGroup mSex;
    @BindView(R.id.marketing)
    protected CheckBox mMarketing;
    private ApplicationSettings settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        settings = new ApplicationSettings(this);

        mName.setText(settings.getName());
        mLastName.setText(settings.getLastName());
        mMarketing.setChecked(settings.isMarketing());
        if (settings.getSex() != null) {
            if (settings.getSex().equalsIgnoreCase("F")) mSex.check(R.id.sex_woman);
            if (settings.getSex().equalsIgnoreCase("M")) mSex.check(R.id.sex_man);
        }
    }

    @OnClick(R.id.save)
    protected void onSave() {
        settings.setName(mName.getText().toString());
        settings.setLastName(mLastName.getText().toString());
        settings.setMarketing(mMarketing.isChecked());
        settings.setSex(mSex.getCheckedRadioButtonId() == R.id.sex_woman ? "F" : "M");
    }

    @OnClick(R.id.write_internal)
    protected void onWriteInternal() {
        try {
            File internalFile = getInternalFile();

            writeToFile(internalFile);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void writeToFile(File internalFile) throws IOException {
        FileUtils.write(internalFile,
                settings.getName() + " " +
                        settings.getLastName() + " " +
                        settings.getSex());
    }

    @NonNull
    private File getInternalFile() throws IOException {
        // getFilesDir zwraca wskazanie do katalogu prywatnego naszej aplikacji
        File internalFile = new File(getFilesDir(), "userdata.txt");
        if (!internalFile.exists()) {
            internalFile.createNewFile();
        }
        return internalFile;
    }

    @OnClick(R.id.read_internal)
    protected void onReadInternal() {
        try {
            File internalFile = getInternalFile();

            readFromFile(internalFile);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void readFromFile(File internalFile) throws IOException {
        String readString = FileUtils.readFileToString(internalFile);
        Toast.makeText(this, readString, Toast.LENGTH_LONG).show();
    }

    private boolean isExternalReadable() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED_READ_ONLY)
                || isExternalWritable();
    }

    private boolean isExternalWritable() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    @OnClick(R.id.write_external)
    protected void onWriteExternal() {
        if (isExternalWritable()) {
            try {
                File externalFile = getExternalFile();
                if (!externalFile.exists()) {
                    externalFile.createNewFile();
                }

                writeToFile(externalFile);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    @NonNull
    private File getExternalFile() {
        File appFolder = new File(Environment.getExternalStorageDirectory(), "szkolenie");
        appFolder.mkdirs();

        return new File(appFolder, "userdata.txt");
    }

    @OnClick(R.id.read_external)
    protected void onReadExternal() {
        if (isExternalReadable()) {
            File externalFile = getExternalFile();

            try {
                readFromFile(externalFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
