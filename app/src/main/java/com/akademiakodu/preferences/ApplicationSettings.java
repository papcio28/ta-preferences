package com.akademiakodu.preferences;

import android.content.Context;
import android.content.SharedPreferences;

public class ApplicationSettings {
    public static final String PREFERENCES_NAME = "settings";
    public static final String KEY_NAME = "name";
    private static final String KEY_LAST_NAME = "lastname";
    private static final String KEY_SEX = "sex";
    private static final String KEY_MARKETING = "marketing";

    private final SharedPreferences mPreferences;

    public ApplicationSettings(Context context) {
        this.mPreferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public String getName() {
        return mPreferences.getString(KEY_NAME, null);
    }

    public void setName(String name) {
        mPreferences.edit().putString(KEY_NAME, name).apply();
    }

    public String getLastName() {
        return mPreferences.getString(KEY_LAST_NAME, null);
    }

    public void setLastName(String lastname) {
        mPreferences.edit().putString(KEY_LAST_NAME, lastname).apply();
    }

    public String getSex() {
        return mPreferences.getString(KEY_SEX, null);
    }

    public void setSex(String sex) {
        mPreferences.edit().putString(KEY_SEX, sex).apply();
    }

    public boolean isMarketing() {
        return mPreferences.getBoolean(KEY_MARKETING, false);
    }

    public void setMarketing(boolean marketing) {
        mPreferences.edit().putBoolean(KEY_MARKETING, marketing).apply();
    }
}
